extern alias References;
using System;
using System.Globalization;
using System.Text.RegularExpressions;
using References::Newtonsoft.Json;
using References::ProtoBuf;
using uMod.Auth;
using uMod.Common;
using uMod.Configuration.Toml;
using uMod.Text;
using uMod.Unity;
using UnityEngine;

namespace uMod.Game.Rust
{
    /// <summary>
    /// Represents a player, either connected or not
    /// </summary>
    public class RustPlayer : UniversalPlayer, IPlayer
    {
        #region Initialization

        private const string ipPattern = @":{1}[0-9]{1}\d*";
        private readonly ulong steamId;

        internal BasePlayer basePlayer;

        public RustPlayer(string playerId, string playerName)
        {
            // Store player details
            Id = playerId;
            Name = playerName.Sanitize();
            ulong.TryParse(playerId, out steamId);
        }

        public RustPlayer(BasePlayer basePlayer) : this(basePlayer.UserIDString, basePlayer.displayName)
        {
            // Store player object
            this.basePlayer = basePlayer;
        }

        #endregion Initialization

        #region Objects

        /// <summary>
        /// Gets/sets the object that backs the player
        /// </summary>
        [JsonIgnore, ProtoIgnore, TomlIgnore]
        public object Object { get; set; }

        /// <summary>
        /// Gets the player's last command type
        /// </summary>
        [JsonIgnore, ProtoIgnore, TomlIgnore]
        public CommandType LastCommand { get; set; }

        /// <summary>
        /// Reconnects the gamePlayer to the player object
        /// </summary>
        /// <param name="gamePlayer"></param>
        public void Reconnect(object gamePlayer)
        {
            // Reconnect player objects
            Object = gamePlayer;
            basePlayer = gamePlayer as BasePlayer;
        }

        #endregion Objects

        #region Information

        /// <summary>
        /// Gets/sets the name for the player
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the ID for the player (unique within the current game)
        /// </summary>
        public override string Id { get; }

        /// <summary>
        /// Gets the language for the player
        /// </summary>
        public CultureInfo Language => CultureInfo.GetCultureInfo(basePlayer != null ? basePlayer?.net?.connection?.info.GetString("global.language") : "en");

        /// <summary>
        /// Gets the IP address for the player
        /// </summary>
        public string Address => basePlayer?.net?.connection != null ? Regex.Replace(basePlayer.net.connection.ipaddress, ipPattern, "") : string.Empty;

        /// <summary>
        /// Gets the average network ping for the player
        /// </summary>
        public int Ping => basePlayer?.net?.connection != null ? Network.Net.sv.GetAveragePing(basePlayer.net.connection) : 0;

        /// <summary>
        /// Gets if the player is a server admin
        /// </summary>
        public override bool IsAdmin => ServerUsers.Is(steamId, ServerUsers.UserGroup.Owner);

        /// <summary>
        /// Gets if the player is a server moderator
        /// </summary>
        public override bool IsModerator => ServerUsers.Is(steamId, ServerUsers.UserGroup.Moderator);

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        public bool IsBanned => ServerUsers.Is(steamId, ServerUsers.UserGroup.Banned);

        /// <summary>
        /// Gets if the player is connected
        /// </summary>
        public bool IsConnected => basePlayer != null ? basePlayer.IsConnected : BasePlayer.FindByID(steamId) != null;

        /// <summary>
        /// Gets if the player is alive
        /// </summary>
        public bool IsAlive => basePlayer != null && basePlayer.IsAlive();

        /// <summary>
        /// Gets if the player is dead
        /// </summary>
        public bool IsDead => basePlayer != null && basePlayer.IsDead();

        /// <summary>
        /// Gets if the player is sleeping
        /// </summary>
        public bool IsSleeping => BasePlayer.FindSleeping(steamId) != null;

        /// <summary>
        /// Gets if the player is the server
        /// </summary>
        public bool IsServer => false;

        /// <summary>
        /// Gets/sets if the player has connected before
        /// </summary>
        public bool IsReturningPlayer { get; set; }

        /// <summary>
        /// Gets/sets if the player has spawned before
        /// </summary>
        public bool HasSpawnedBefore { get; set; }

        #endregion Information

        #region Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string reason = "", TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned)
            {
                // Ban and kick player
                ServerUsers.Set(steamId, ServerUsers.UserGroup.Banned, basePlayer?.displayName ?? "Unknown", reason); // TODO: Localization
                ServerUsers.Save();

                // Kick player with reason
                Kick(reason);
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        public TimeSpan BanTimeRemaining => IsBanned ? TimeSpan.MaxValue : TimeSpan.Zero;

        /// <summary>
        /// Kicks the player from the server
        /// </summary>
        /// <param name="reason"></param>
        public void Kick(string reason = "")
        {
            if (IsConnected)
            {
                basePlayer.Kick(reason);
            }
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        public void Unban()
        {
            // Check if already unbanned
            if (IsBanned)
            {
                // Unban player
                ServerUsers.Remove(steamId);
                ServerUsers.Save();
            }
        }

        #endregion Administration

        #region Character

        /// <summary>
        /// Gets/sets the player's health
        /// </summary>
        public float Health
        {
            get => basePlayer != null ? basePlayer.health : 0f;
            set
            {
                if (basePlayer != null)
                {
                    basePlayer.health = value > 0f ? value : 0f;
                }
            }
        }

        /// <summary>
        /// Gets/sets the player's maximum health
        /// </summary>
        public float MaxHealth
        {
            get => basePlayer != null ? basePlayer.MaxHealth() : 0f;
            set
            {
                if (basePlayer != null)
                {
                    basePlayer._maxHealth = value;
                }
            }
        }

        /// <summary>
        /// Heals the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Heal(float amount)
        {
            if (basePlayer != null)
            {
                basePlayer.metabolism.bleeding.value = 0;
                basePlayer.metabolism.calories.value += amount;
                basePlayer.metabolism.dirtyness.value = 0;
                basePlayer.metabolism.hydration.value += amount;
                basePlayer.metabolism.oxygen.value = 1;
                basePlayer.metabolism.poison.value = 0;
                basePlayer.metabolism.radiation_level.value = 0;
                basePlayer.metabolism.radiation_poison.value = 0;
                basePlayer.metabolism.temperature.value = 32;
                basePlayer.metabolism.wetness.value = 0;
                basePlayer.StopWounded();
                basePlayer.Heal(amount);
            }
        }

        /// <summary>
        /// Damages the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Hurt(float amount) => basePlayer?.Hurt(amount);

        /// <summary>
        /// Causes the player's character to die
        /// </summary>
        public void Kill() => basePlayer?.Die();

        /// <summary>
        /// Renames the player to specified name
        /// <param name="newName"></param>
        /// </summary>
        public void Rename(string newName)
        {
            if (basePlayer?.net?.connection != null)
            {
                // Clean up name
                newName = string.IsNullOrEmpty(newName.Trim()) ? basePlayer.displayName : newName;

                // Update name in-game
                basePlayer.net.connection.username = newName;
                basePlayer.displayName = newName;
                basePlayer._name = newName;
                basePlayer.SendNetworkUpdateImmediate();

                // Update name in uMod
                Name = newName;
            }
        }

        /// <summary>
        /// Resets the player's character stats
        /// </summary>
        public void Reset()
        {
            if (basePlayer != null)
            {
                basePlayer.StopWounded();
                basePlayer.metabolism.Reset();
                basePlayer.metabolism.SendChangesToClient();
            }
        }

        /// <summary>
        /// Respawns the player's character
        /// </summary>
        public void Respawn() => basePlayer?.Respawn();

        /// <summary>
        /// Respawns the player's character at specified position
        /// </summary>
        public void Respawn(Position pos)
        {
            if (basePlayer != null)
            {
                basePlayer.RespawnAt(pos.ToVector3(), new Quaternion());
            }
        }

        #endregion Character

        #region Positional

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <returns></returns>
        public Position Position() => basePlayer?.transform?.position.ToPosition();

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Position(out float x, out float y, out float z)
        {
            Position pos = Position();
            x = pos.X;
            y = pos.Y;
            z = pos.Z;
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Teleport(float x, float y, float z)
        {
            if (IsConnected && basePlayer.IsAlive() && !basePlayer.IsSpectating())
            {
                try
                {
                    Vector3 destination = new Vector3(x, y, z);

                    // Dismount and remove parent, if applicable
                    basePlayer.EnsureDismounted();
                    basePlayer.SetParent(null, true, true);

                    // Prevent player from getting hurt
                    //basePlayer.SetPlayerFlag(BasePlayer.PlayerFlags.ReceivingSnapshot, true);
                    //basePlayer.UpdatePlayerCollider(true);
                    //basePlayer.UpdatePlayerRigidbody(false);
                    basePlayer.SetServerFall(true);

                    // Teleport the player to position
                    basePlayer.MovePosition(destination);
                    basePlayer.ClientRPCPlayer(null, basePlayer, "ForcePositionTo", destination);

                    // Update network group if outside current group
                    /*if (!basePlayer.net.sv.visibility.IsInside(basePlayer.net.group, destination))
                    {
                        basePlayer.UpdateNetworkGroup();
                        basePlayer.SendNetworkUpdateImmediate();
                        basePlayer.ClearEntityQueue();
                        basePlayer.SendFullSnapshot();
                    }*/
                }
                finally
                {
                    // Restore player behavior
                    //basePlayer.UpdatePlayerCollider(true);
                    //basePlayer.UpdatePlayerRigidbody(true);
                    basePlayer.SetServerFall(false);
                }
            }
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="pos"></param>
        public void Teleport(Position pos) => Teleport(pos.X, pos.Y, pos.Z);

        #endregion Positional

        #region Chat and Commands

        /// <summary>
        /// Sends the specified message and prefix to the player
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Message(string message, string prefix, params object[] args)
        {
            if (!string.IsNullOrEmpty(message) && IsConnected)
            {
                message = args.Length > 0 ? string.Format(Formatter.ToUnity(message), args) : Formatter.ToUnity(message);
                basePlayer.SendConsoleCommand("chat.add", 2, RustProvider.Instance.Config.Chat.Avatar, prefix != null ? $"{prefix} {message}" : message); // TODO: Implement setting for avatar
            }
        }

        /// <summary>
        /// Sends the specified message to the player
        /// </summary>
        /// <param name="message"></param>
        public void Message(string message) => Message(message, null);

        /// <summary>
        /// Replies to the player with the specified message and prefix
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Reply(string message, string prefix, params object[] args)
        {
            if (!string.IsNullOrEmpty(message) && IsConnected)
            {
                switch (LastCommand)
                {
                    case CommandType.Chat:
                        Message(message, prefix, args);
                        break;

                    case CommandType.Console:
                        basePlayer.ConsoleMessage(string.Format(Formatter.ToPlaintext(message), args));
                        break;
                }
            }
        }

        /// <summary>
        /// Replies to the player with the specified message
        /// </summary>
        /// <param name="message"></param>
        public void Reply(string message) => Reply(message, null);

        /// <summary>
        /// Runs the specified console command on the player
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            if (!string.IsNullOrEmpty(command) && IsConnected)
            {
                basePlayer.SendConsoleCommand(command, args);
            }
        }

        #endregion Chat and Commands

        #region Item Handling

        /// <summary>
        /// Drops item by item ID from player's inventory
        /// </summary>
        /// <param name="itemId"></param>
        /*public void DropItem(int itemId)
        {
            Vector3 position = player.transform.position;
            PlayerInventory inventory = Inventory(player);
            for (int s = 0; s < inventory.containerMain.capacity; s++)
            {
                global::Item i = inventory.containerMain.GetSlot(s);
                if (i.info.itemid == itemId)
                {
                    i.Drop(position + new Vector3(0f, 1f, 0f) + position / 2f, (position + new Vector3(0f, 0.2f, 0f)) * 8f);
                }
            }
            for (int s = 0; s < inventory.containerBelt.capacity; s++)
            {
                global::Item i = inventory.containerBelt.GetSlot(s);
                if (i.info.itemid == itemId)
                {
                    i.Drop(position + new Vector3(0f, 1f, 0f) + position / 2f, (position + new Vector3(0f, 0.2f, 0f)) * 8f);
                }
            }
            for (int s = 0; s < inventory.containerWear.capacity; s++)
            {
                global::Item i = inventory.containerWear.GetSlot(s);
                if (i.info.itemid == itemId)
                {
                    i.Drop(position + new Vector3(0f, 1f, 0f) + position / 2f, (position + new Vector3(0f, 0.2f, 0f)) * 8f);
                }
            }
        }*/

        /// <summary>
        /// Drops item from the player's inventory
        /// </summary>
        /// <param name="item"></param>
        /*public void DropItem(global::Item item)
        {
            Vector3 position = player.transform.position;
            PlayerInventory inventory = Inventory(player);
            for (int s = 0; s < inventory.containerMain.capacity; s++)
            {
                global::Item i = inventory.containerMain.GetSlot(s);
                if (i == item)
                {
                    i.Drop(position + new Vector3(0f, 1f, 0f) + position / 2f, (position + new Vector3(0f, 0.2f, 0f)) * 8f);
                }
            }
            for (int s = 0; s < inventory.containerBelt.capacity; s++)
            {
                global::Item i = inventory.containerBelt.GetSlot(s);
                if (i == item)
                {
                    i.Drop(position + new Vector3(0f, 1f, 0f) + position / 2f, (position + new Vector3(0f, 0.2f, 0f)) * 8f);
                }
            }
            for (int s = 0; s < inventory.containerWear.capacity; s++)
            {
                global::Item i = inventory.containerWear.GetSlot(s);
                if (i == item)
                {
                    i.Drop(position + new Vector3(0f, 1f, 0f) + position / 2f, (position + new Vector3(0f, 0.2f, 0f)) * 8f);
                }
            }
        }*/

        /// <summary>
        /// Gives quantity of an item to the player
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="quantity"></param>
        //public void GiveItem(int itemId, int quantity = 1) => GiveItem(player, Item.GetItem(itemId), quantity);

        /// <summary>
        /// Gives quantity of an item to the player
        /// </summary>
        /// <param name="item"></param>
        /// <param name="quantity"></param>
        //public void GiveItem(global::Item item, int quantity = 1) => player.inventory.GiveItem(ItemManager.CreateByItemID(item.info.itemid, quantity));

        #endregion Item Handling

        #region Inventory Handling

        /// <summary>
        /// Gets the inventory of the player
        /// </summary>
        /// <returns></returns>
        //public PlayerInventory Inventory() => player.inventory;

        /// <summary>
        /// Clears the inventory of the player
        /// </summary>
        //public void ClearInventory() => Inventory(player)?.Strip();

        /// <summary>
        /// Resets the inventory of the player
        /// </summary>
        /*public void ResetInventory()
        {
            PlayerInventory inventory = Inventory(player);
            if (inventory != null)
            {
                inventory.DoDestroy();
                inventory.ServerInit(player);
            }
        }*/

        #endregion Inventory Handling
    }
}
