using Facepunch;
using System;
using System.Globalization;
using System.IO;
using System.Net;
using uMod.Common;
using uMod.IO;
using uMod.Text;
using WebResponse = uMod.Common.Web.WebResponse;

namespace uMod.Game.Rust
{
    /// <summary>
    /// Represents the server hosting the game instance
    /// </summary>
    public class RustServer : IServer
    {
        /// <summary>
        /// Gets or sets the player manager
        /// </summary>
        public IPlayerManager PlayerManager { get; set; }

        #region Server Information

        /// <summary>
        /// Gets/sets the public-facing name of the server
        /// </summary>
        public string Name
        {
            get => ConVar.Server.hostname;
            set => ConVar.Server.hostname = value;
        }

        private static IPAddress address;
        private static IPAddress localAddress;

        /// <summary>
        /// Gets the public-facing IP address of the server, if known
        /// </summary>
        public IPAddress Address
        {
            get
            {
                try
                {
                    if (address == null || !Utility.ValidateIPv4(address.ToString()))
                    {
                        if (Utility.ValidateIPv4(ConVar.Server.ip) && !Utility.IsLocalIP(ConVar.Server.ip))
                        {
                            IPAddress.TryParse(ConVar.Server.ip, out address);
                            Interface.uMod.LogInfo($"IP address from server: {address}"); // TODO: Localization
                        }
                        else
                        {
                            Web.Client webClient = new Web.Client();
                            webClient.Get("https://api.ipify.org")
                               .Done(delegate (WebResponse response)
                               {
                                   if (response.StatusCode == 200)
                                   {
                                       IPAddress.TryParse(response.ReadAsString(), out address);
                                       Interface.uMod.LogInfo($"IP address from external API: {address}"); // TODO: Localization
                                   }
                               });
                        }
                    }

                    return address;
                }
                catch (Exception ex)
                {
                    Interface.uMod.LogWarning("Couldn't get server's public IP address", ex); // TODO: Localization
                    return IPAddress.Any;
                }
            }
        }

        /// <summary>
        /// Gets the local IP address of the server, if known
        /// </summary>
        public IPAddress LocalAddress
        {
            get
            {
                try
                {
                    return localAddress ?? (localAddress = Utility.GetLocalIP());
                }
                catch
                {
                    return IPAddress.Any;
                }
            }
        }

        /// <summary>
        /// Gets the public-facing network port of the server, if known
        /// </summary>
        public ushort Port => (ushort)ConVar.Server.port;

        /// <summary>
        /// Gets the version or build number of the server
        /// </summary>
        public string Version => BuildInfo.Current.Build.Number;

        /// <summary>
        /// Gets the network protocol version of the server
        /// </summary>
        public string Protocol => global::Rust.Protocol.printable;

        /// <summary>
        /// Gets the language set by the server
        /// </summary>
        public CultureInfo Language => CultureInfo.InstalledUICulture;

        /// <summary>
        /// Gets the total of players currently on the server
        /// </summary>
        public int Players => BasePlayer.activePlayerList.Count;

        /// <summary>
        /// Gets/sets the maximum players allowed on the server
        /// </summary>
        public int MaxPlayers
        {
            get => ConVar.Server.maxplayers;
            set => ConVar.Server.maxplayers = value;
        }

        /// <summary>
        /// Gets/sets the current in-game time on the server
        /// </summary>
        public DateTime Time
        {
            get => TOD_Sky.Instance.Cycle.DateTime;
            set => TOD_Sky.Instance.Cycle.DateTime = value;
        }

        /// <summary>
        /// Gets the current or average frame rate of the server
        /// </summary>
        public int FrameRate
        {
            get => Performance.report.frameRate;
        }

        /// <summary>
        /// Gets/sets the target frame rate for the server
        /// </summary>
        public int TargetFrameRate
        {
            get => UnityEngine.Application.targetFrameRate;
            set => UnityEngine.Application.targetFrameRate = value;
        }

        /// <summary>
        /// Gets information on the currently loaded save file
        /// </summary>
        ISaveInfo IServer.SaveInfo => SaveInfo.Create(World.SaveFileName);

        #endregion Server Information

        #region Server Administration

        /// <summary>
        /// Saves the server and any related information
        /// </summary>
        public void Save()
        {
            ConVar.Server.save(null);
            File.WriteAllText(Path.Combine(ConVar.Server.GetServerFolder("cfg"), "serverauto.cfg"), ConsoleSystem.SaveToConfigString(true));
            ServerUsers.Save();
        }

        /// <summary>
        /// Shuts down the server, with optional saving and delay
        /// </summary>
        public void Shutdown(bool save = true, int delay = 0)
        {
            // TODO: Implement optional save and delay
            ConVar.Global.quit(null);
        }

        #endregion Server Administration

        #region Player Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string playerId, string reason = "", TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned(playerId))
            {
                // Ban player
                ServerUsers.Set(ulong.Parse(playerId), ServerUsers.UserGroup.Banned, Name, reason);
                ServerUsers.Save();

                // TODO: Implement universal ban storage

                // TODO: Kick player if connected
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        /// <param name="playerId"></param>
        public TimeSpan BanTimeRemaining(string playerId) => IsBanned(playerId) ? TimeSpan.MaxValue : TimeSpan.Zero;

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        /// <param name="playerId"></param>
        public bool IsBanned(string playerId) => ServerUsers.Is(ulong.Parse(playerId), ServerUsers.UserGroup.Banned);

        /// <summary>
        /// Unbans the player
        /// </summary>
        /// <param name="playerId"></param>
        public void Unban(string playerId)
        {
            // Check if already unbanned
            if (IsBanned(playerId))
            {
                // Unban player
                ServerUsers.Remove(ulong.Parse(playerId));
                ServerUsers.Save();

                // TODO: Implement universal ban storage
            }
        }

        #endregion Player Administration

        #region Chat and Commands

        /// <summary>
        /// Broadcasts the specified chat message and prefix to all players with the specified icon
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="id"></param>
        /// <param name="args"></param>
        public void Broadcast(string message, string prefix, ulong id, params object[] args)
        {
            if (!string.IsNullOrEmpty(message))
            {
                message = args.Length > 0 ? string.Format(Formatter.ToUnity(message), args) : Formatter.ToUnity(message);
                ConsoleNetwork.BroadcastToAllClients("chat.add", 2, id == 0 ? RustProvider.Instance.Config.Chat.Avatar : id, !string.IsNullOrEmpty(prefix) ? $"{prefix}: {message}" : message);
            }
        }

        /// <summary>
        /// Broadcasts the specified chat message and prefix to all players
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Broadcast(string message, string prefix, params object[] args)
        {
            Broadcast(message, prefix, RustProvider.Instance.Config.Chat.Avatar, args);
        }

        /// <summary>
        /// Broadcasts the specified chat message to all players with the specified icon
        /// </summary>
        /// <param name="message"></param>
        /// <param name="id"></param>
        /// <param name="args"></param>
        public void Broadcast(string message, ulong id, params object[] args)
        {
            Broadcast(message, string.Empty, id, args);
        }

        /// <summary>
        /// Broadcasts the specified chat message to all players
        /// </summary>
        /// <param name="message"></param>
        public void Broadcast(string message) => Broadcast(message, null);

        /// <summary>
        /// Runs the specified server command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            if (!string.IsNullOrEmpty(command))
            {
                ConsoleSystem.Run(ConsoleSystem.Option.Server, command, args);
            }
        }

        #endregion Chat and Commands
    }
}
