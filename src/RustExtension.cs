using System;
using System.Collections.Generic;
using System.Reflection;
using uMod.Common;
using uMod.Extensions;

namespace uMod.Game.Rust
{
    /// <summary>
    /// The extension class that represents this extension
    /// </summary>
    [GameExtension]
    public class RustExtension : Extension
    {
        // Get assembly info
        internal static Assembly Assembly = Assembly.GetExecutingAssembly();
        internal static AssemblyName AssemblyName = Assembly.GetName();
        internal static VersionNumber AssemblyVersion = new VersionNumber(AssemblyName.Version.Major, AssemblyName.Version.Minor, AssemblyName.Version.Build);
        internal static string AssemblyAuthors = ((AssemblyCompanyAttribute)Attribute.GetCustomAttribute(Assembly, typeof(AssemblyCompanyAttribute), false)).Company;

        /// <summary>
        /// Gets whether this extension is for a specific game
        /// </summary>
        public override bool IsGameExtension => true;

        /// <summary>
        /// Gets the title of this extension
        /// </summary>
        public override string Title => "Rust";

        /// <summary>
        /// Gets the author of this extension
        /// </summary>
        public override string Author => AssemblyAuthors;

        /// <summary>
        /// Gets the version of this extension
        /// </summary>
        public override VersionNumber Version => AssemblyVersion;

        /// <summary>
        /// Gets the branch of this extension
        /// </summary>
        public override string Branch => "public"; // TODO: Handle this programmatically

        /// <summary>
        /// Commands that plugins are prevented from overriding
        /// </summary>
        internal static IEnumerable<string> RestrictedCommands => new[]
        {
            "ownerid",
            "moderatorid",
            "removeowner",
            "removemoderator"
        };

        /// <summary>
        /// Default game-specific references for use in plugins
        /// </summary>
        public override string[] DefaultReferences => new[]
        {
            "ApexAI",
            "ApexShared",
            "Facepunch.Network",
            "Facepunch.Steamworks.Posix64",
            "Facepunch.Steamworks.Win64",
            "Facepunch.System",
            "Facepunch.UnityEngine",
            "NewAssembly",
            "Rust.Data",
            "Rust.Global",
            "Rust.Localization",
            "Rust.Platform",
            "Rust.Platform.Common",
            "Rust.Workshop",
            "Rust.World",
            "System.Drawing",
            "netstandard"
        };

        /// <summary>
        /// List of namespaces allowed for use in plugins
        /// </summary>
        public override string[] WhitelistedNamespaces => new[]
        {
            "ConVar",
            "Dest",
            "Facepunch",
            "Network",
            "PVT",
            "Rust",
            "Steamworks"
        };

        /// <summary>
        /// List of namespaces blacklisted for use in plugins
        /// </summary>
        public override string[] BlacklistedNamespaces => new[]
        {
            "RawWriter",
            "ServerMgr.UpdateNetworkInformation",
            "Facepunch.Steamworks.Server"
        };

        /// <summary>
        /// Initializes a new instance of the RustExtension class
        /// </summary>
        public RustExtension()
        {
        }

        /// <summary>
        /// Called when this extension has been added to the specified manager
        /// </summary>
        /// <param name="manager"></param>
        public override void HandleAddedToManager(IExtensionManager manager)
        {
            manager.RegisterPluginLoader(new RustPluginLoader(Interface.uMod.RootLogger));

#pragma warning disable 612
            manager.RegisterLibrary("Command", new Libraries.Command(Interface.uMod.Application));
            manager.RegisterLibrary("Player", new Libraries.Player(Interface.uMod.Application));
            manager.RegisterLibrary("Server", new Libraries.Server(Interface.uMod.Application));
            manager.RegisterLibrary("Rust", new Libraries.Rust(Interface.uMod.Application));
#pragma warning restore 612
        }
    }
}
