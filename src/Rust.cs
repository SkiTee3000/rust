using ConVar;
using Facepunch;
using Network;
using Rust.Ai;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using uMod.Common;
using uMod.Configuration;
using uMod.Plugins;
using uMod.Plugins.Decorators;
using uMod.Pooling;
using UnityEngine;

namespace uMod.Game.Rust
{
    /// <summary>
    /// The core Rust plugin
    /// </summary>
    [HookDecorator(typeof(ServerDecorator))]
    public class Rust : Plugin
    {
        #region Initialization

        internal static ulong chatAvatar = 76561198127163614; // TODO: Implement setting for avatar

        private static readonly RustProvider Universal = RustProvider.Instance;
        private static readonly string ipPattern = @":{1}[0-9]{1}\d*";

        private bool isPlayerTakingDamage;
        private bool serverInitialized;

        /// <summary>
        /// Initializes a new instance of the Rust class
        /// </summary>
        public Rust()
        {
            // Set plugin info attributes
            Title = "Rust";
            Author = RustExtension.AssemblyAuthors;
            Version = RustExtension.AssemblyVersion;
        }

        #endregion Initialization

        #region Modifications

        // Set default values for command-line arguments and hide output
        [Hook("IOnRunCommandLine")]
        private object IOnRunCommandLine()
        {
            foreach (KeyValuePair<string, string> pair in Facepunch.CommandLine.GetSwitches())
            {
                string value = pair.Value;
                if (value == "")
                {
                    value = "1";
                }

                string str = pair.Key.Substring(1);
                ConsoleSystem.Option options = ConsoleSystem.Option.Unrestricted;
                options.PrintOutput = false;
                ConsoleSystem.Run(options, str, value);
            }
            return true;
        }

        #endregion Modifications

        #region Entity Hooks

        /// <summary>
        /// Called when a BaseCombatEntity takes damage
        /// This is used to call OnEntityTakeDamage for anything other than a BasePlayer
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="hitInfo"></param>
        /// <returns></returns>
        [Hook("IOnBaseCombatEntityHurt")]
        private object IOnBaseCombatEntityHurt(BaseCombatEntity entity, HitInfo hitInfo)
        {
            return entity is BasePlayer ? null : Interface.CallHook("OnEntityTakeDamage", entity, hitInfo);
        }

        /// <summary>
        /// Called when an NPC animal tries to target an entity
        /// </summary>
        /// <param name="npc"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        [Hook("IOnNpcTarget")]
        private object IOnNpcTarget(BaseNpc npc, BaseEntity target)
        {
            if (Interface.CallHook("OnNpcTarget", npc, target) != null)
            {
                npc.SetFact(BaseNpc.Facts.HasEnemy, 0);
                npc.SetFact(BaseNpc.Facts.EnemyRange, 3);
                npc.SetFact(BaseNpc.Facts.AfraidRange, 1);

                //TODO: Find replacements of those:
                // npc.AiContext.EnemyPlayer = null;
                // npc.AiContext.LastEnemyPlayerScore = 0f;

                npc.playerTargetDecisionStartTime = 0f;
                return 0f;
            }

            return null;
        }

        /// <summary>
        /// Called after a BaseNetworkable has been saved into a ProtoBuf object that is about to
        /// be serialized for a network connection or cache
        /// </summary>
        /// <param name="baseNetworkable"></param>
        /// <param name="saveInfo"></param>
        [HookMethod("IOnEntitySaved")]
        private void IOnEntitySaved(BaseNetworkable baseNetworkable, BaseNetworkable.SaveInfo saveInfo)
        {
            // Only call when saving for the network since we don't expect plugins to want to intercept saving to disk
            if (!serverInitialized || saveInfo.forConnection == null)
            {
                return;
            }

            Interface.CallHook("OnEntitySaved", baseNetworkable, saveInfo);
        }

        #endregion Entity Hooks

        #region Item Hooks

        /// <summary>
        /// Called when an item loses durability
        /// </summary>
        /// <param name="item"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        [Hook("IOnLoseCondition")]
        private object IOnLoseCondition(Item item, float amount)
        {
            // Call hook for plugins
            Interface.CallHook("OnLoseCondition", item, amount);

            float condition = item.condition;
            item.condition -= amount;
            if (item.condition <= 0f && item.condition < condition)
            {
                item.OnBroken();
            }

            return true;
        }

        #endregion Item Hooks

        #region Player Hooks

        /// <summary>
        /// Called when a player attempts to pickup a DoorCloser entity
        /// </summary>
        /// <param name="player"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [Hook("ICanPickupEntity")]
        private object ICanPickupEntity(BasePlayer player, DoorCloser entity)
        {
            return Interface.CallHook("CanPickupEntity", player, entity) is bool result && !result ? (object)true : null;
        }

        /// <summary>
        /// Called when a BasePlayer is attacked
        /// This is used to call OnEntityTakeDamage for a BasePlayer when attacked
        /// </summary>
        /// <param name="player"></param>
        /// <param name="hitInfo"></param>
        [Hook("IOnBasePlayerAttacked")]
        private object IOnBasePlayerAttacked(BasePlayer player, HitInfo hitInfo)
        {
            if (!serverInitialized || player == null || hitInfo == null || player.IsDead() || isPlayerTakingDamage || player is NPCPlayer)
            {
                return null;
            }

            if (Interface.CallHook("OnEntityTakeDamage", player, hitInfo) != null)
            {
                return true;
            }

            isPlayerTakingDamage = true;
            try
            {
                player.OnAttacked(hitInfo);
            }
            finally
            {
                isPlayerTakingDamage = false;
            }
            return true;
        }

        /// <summary>
        /// Called when a BasePlayer is hurt
        /// This is used to call OnEntityTakeDamage when the player was hurt without being attacked
        /// </summary>
        /// <param name="player"></param>
        /// <param name="hitInfo"></param>
        /// <returns></returns>
        [Hook("IOnBasePlayerHurt")]
        private object IOnBasePlayerHurt(BasePlayer player, HitInfo hitInfo)
        {
            return isPlayerTakingDamage ? null : Interface.CallHook("OnEntityTakeDamage", player, hitInfo);
        }

        /// <summary>
        /// Called when a server group is set for an ID (i.e. banned)
        /// </summary>
        /// <param name="steamId"></param>
        /// <param name="group"></param>
        /// <param name="name"></param>
        /// <param name="reason"></param>
        /// <param name="expiry"></param>
        [Hook("IOnServerUsersSet")]
        private void IOnServerUsersSet(ulong steamId, ServerUsers.UserGroup group, string name, string reason, long expiry)
        {
            if (serverInitialized)
            {
                if (group == ServerUsers.UserGroup.Banned)
                {
                    string playerId = steamId.ToString();
                    IPlayer player = Players.FindPlayerById(playerId);
                    if (player != null)
                    {
                        // Call hook for plugins
                        Interface.CallHook("OnPlayerBanned", player, reason);
                    }
                    Interface.CallHook("OnPlayerBanned", name, playerId, player?.Address ?? string.Empty, reason, expiry);
                    Interface.CallDeprecatedHook("OnUserBanned", "OnPlayerBanned(string playerName, string playerId, string ipAddress)",
                        new DateTime(2021, 12, 31), name, playerId, player?.Address ?? string.Empty, reason, expiry);
                }
            }
        }

        /// <summary>
        /// Called when a server group is removed for an ID (i.e. unbanned)
        /// </summary>
        /// <param name="steamId"></param>
        [Hook("IOnServerUsersRemove")]
        private void IOnServerUsersRemove(ulong steamId)
        {
            if (serverInitialized)
            {
                if (ServerUsers.Is(steamId, ServerUsers.UserGroup.Banned))
                {
                    string playerId = steamId.ToString();
                    IPlayer player = Players.FindPlayerById(playerId);
                    if (player != null)
                    {
                        // Call hook for plugins
                        Interface.CallHook("OnPlayerUnbanned", player);
                    }
                    Interface.CallHook("OnPlayerUnbanned", player?.Name ?? "Unnamed", playerId, player?.Address ?? string.Empty); // TODO: Localization
                    Interface.CallDeprecatedHook("OnUserUnbanned", "OnPlayerUnbanned(string playerName, string playerId, string ipAddress)",
                        new DateTime(2021, 12, 31), player?.Name ?? "Unnamed", playerId, player?.Address ?? string.Empty); // TODO: Localization
                }
            }
        }

        /// <summary>
        /// Called when the player is attempting to connect
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        [Hook("IOnPlayerApprove")]
        private object IOnPlayerApprove(Connection connection)
        {
            string playerName = connection.username;
            string playerId = connection.userid.ToString();
            string ipAddress = Regex.Replace(connection.ipaddress, ipPattern, string.Empty);

            Players.PlayerJoin(playerId, playerName);

            if (Interface.uMod.Auth.Configuration.Groups.AutomaticGrouping)
            {
                // Add player to default groups
                GroupProvider defaultGroups = Interface.uMod.Auth.Configuration.Groups;
                if (connection.authLevel == 2 && !permission.UserHasGroup(playerId, defaultGroups.Administrators))
                {
                    permission.AddUserGroup(playerId, defaultGroups.Administrators);
                }
                else if (connection.authLevel == 1 && !permission.UserHasGroup(playerId, defaultGroups.Moderators))
                {
                    permission.AddUserGroup(playerId, defaultGroups.Moderators);
                }
            }

            // Call pre hook for plugins
            object loginUniversal = Interface.CallHook("CanPlayerLogin", playerName, playerId, ipAddress);
            object loginDeprecated = Interface.CallDeprecatedHook("CanUserLogin", "CanPlayerLogin(string playerName, string playerId, string ipAddress)",
                new DateTime(2021, 12, 31), playerName, playerId, ipAddress);
            object canLogin = loginUniversal is null ? loginDeprecated : loginUniversal;

            // Can the player log in?
            if (canLogin is string || canLogin is bool loginBlocked && !loginBlocked)
            {
                // Reject player with message
                ConnectionAuth.Reject(connection, canLogin is string ? canLogin.ToString() : lang.GetMessage("ConnectionRejected", this, playerId));
                return true;
            }

            // Call post hook for plugins
            Interface.CallHook("OnPlayerApproved", playerName, playerId, ipAddress);
            Interface.CallDeprecatedHook("OnUserApprove", "OnPlayerApproved(string playerName, string playerId, string ipAddress)",
                new DateTime(2021, 12, 31), playerName, playerId, ipAddress);
            Interface.CallDeprecatedHook("OnUserApproved", "OnPlayerApproved(string playerName, string playerId, string ipAddress)",
                new DateTime(2021, 12, 31), playerName, playerId, ipAddress);

            return null;
        }

        /// <summary>
        /// Called when the player has been banned by Publisher/VAC
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="status"></param>
        [Hook("IOnPlayerBanned")]
        private void IOnPlayerBanned(Connection connection, AuthResponse status)
        {
            Interface.CallHook("OnPlayerBanned", connection, status.ToString());
        }

        /// <summary>
        /// Called when the player sends a chat message
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="playerName"></param>
        /// <param name="message"></param>
        /// <param name="channel"></param>
        /// <param name="basePlayer"></param>
        /// <returns></returns>
        [Hook("IOnPlayerChat")]
        private object IOnPlayerChat(ulong playerId, string playerName, string message, Chat.ChatChannel channel, BasePlayer basePlayer)
        {
            // Check if using Rust+ app
            if (basePlayer == null || !basePlayer.IsConnected)
            {
                // Call offline chat hook
                return Interface.CallHook("OnPlayerOfflineChat", playerId, playerName, message, channel);
            }

            // Is the chat message blocked?
            object chatUniversal = Interface.CallHook("OnPlayerChat", basePlayer.IPlayer, message, channel);
            object chatDeprecated = Interface.CallDeprecatedHook("OnUserChat", "OnPlayerChat(IPlayer player, string message)",
                new DateTime(2021, 12, 31), basePlayer.IPlayer, message);
            return chatUniversal is null ? chatDeprecated : chatUniversal;
        }

        /// <summary>
        /// Called when the player sends a chat command
        /// </summary>
        /// <param name="basePlayer"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        [Hook("IOnPlayerCommand")]
        private void IOnPlayerCommand(BasePlayer basePlayer, string command)
        {
            // Check if using Rust+ app
            if (!basePlayer.IsConnected)
            {
                string[] fullCommand = CommandLine.Split(command);
                if (fullCommand.Length >= 1)
                {
                    string cmd = fullCommand[0].ToLower();
                    string[] args = fullCommand.Skip(1).ToArray();

                    Interface.CallHook("OnApplicationCommand", basePlayer.IPlayer, cmd, args);
                }
                return;
            }

            Universal.CommandSystem.HandleChatMessage(basePlayer.IPlayer, command);
        }

        /// <summary>
        /// Called when the player is authenticating
        /// </summary>
        /// <param name="connection"></param>
        [Hook("OnClientAuth")]
        private void OnClientAuth(Connection connection)
        {
            // Strip styling from username
            connection.username = Regex.Replace(connection.username, @"<[^>]*>", string.Empty); // TODO: Move regex pattern outside of hook and optimize
        }

        /// <summary>
        /// Called when the player has connected
        /// </summary>
        /// <param name="basePlayer"></param>
        [Hook("OnPlayerConnected")]
        private void OnPlayerConnected(BasePlayer basePlayer)
        {
            // Set default language for player if not set
            if (string.IsNullOrEmpty(lang.GetLanguage(basePlayer.UserIDString)))
            {
                lang.SetLanguage(basePlayer.net.connection.info.GetString("global.language", "en"), basePlayer.UserIDString);
            }

            // Send CUI to player manually
            basePlayer.SendEntitySnapshot(CommunityEntity.ServerInstance);

            Players.PlayerConnected(basePlayer.UserIDString, basePlayer);

            IPlayer player = Players.FindPlayerById(basePlayer.UserIDString);
            if (player != null)
            {
                // Set IPlayer object in BasePlayer
                basePlayer.IPlayer = player;

                // Call deprecated hooks for plugins
                Interface.CallDeprecatedHook("OnPlayerInit", "OnPlayerConnected", new DateTime(2021, 12, 31), player);
                Interface.CallDeprecatedHook("OnUserConnected", "OnPlayerConnected", new DateTime(2021, 12, 31), player);
            }
        }

        /// <summary>
        /// Called when the player has disconnected
        /// </summary>
        /// <param name="basePlayer"></param>
        /// <param name="reason"></param>
        [Hook("OnPlayerDisconnected")]
        private void OnPlayerDisconnected(BasePlayer basePlayer, string reason)
        {
            IPlayer player = basePlayer.IPlayer;
            if (player != null)
            {
                // Call deprecated hook for plugins
                Interface.CallDeprecatedHook("OnUserDisconnected", "OnPlayerDisconnected", new DateTime(2021, 12, 31), player, reason);
            }

            Players.PlayerDisconnected(basePlayer.UserIDString);
        }

        /// <summary>
        /// Called when setting/changing info values for a player
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="key"></param>
        /// <param name="val"></param>
        [Hook("OnPlayerSetInfo")]
        private void OnPlayerSetInfo(Connection connection, string key, string val)
        {
            // Change language for player
            if (key == "global.language")
            {
                lang.SetLanguage(val, connection.userid.ToString());
            }
        }

        /// <summary>
        /// Called when the player respawns
        /// </summary>
        /// <param name="basePlayer"></param>
        [Hook("OnPlayerRespawn")]
        private void OnPlayerRespawn(BasePlayer basePlayer) => basePlayer.IPlayer.HasSpawnedBefore = true;

        #endregion Player Hooks

        #region Server Hooks

        /// <summary>
        /// Called when a remote console command is received
        /// </summary>
        /// <returns></returns>
        /// <param name="ipAddress"></param>
        /// <param name="command"></param>
        [Hook("IOnRconCommand")]
        private object IOnRconCommand(IPAddress ipAddress, string command)
        {
            string[] fullCommand = CommandLine.Split(command);
            if (fullCommand.Length >= 1)
            {
                string cmd = fullCommand[0].ToLower();
                string[] args = fullCommand.Skip(1).ToArray();

                // Is the RCON command blocked?
                if (Interface.CallHook("OnRconCommand", ipAddress, cmd, args) != null)
                {
                    return true;
                }
            }

            return null;
        }

        /// <summary>
        /// Called when a server command was run
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        [Hook("IOnServerCommand")]
        private object IOnServerCommand(ConsoleSystem.Arg arg)
        {
            if (arg == null || arg.Connection != null && arg.Player() == null)
            {
                return true; // Ignore console commands from client during connection
            }

            if (arg.cmd.FullName == "chat.say" || arg.cmd.FullName == "chat.teamsay")
            {
                return null; // Skip chat commands, those are handled elsewhere
            }

            IPlayer player = arg.Player()?.IPlayer;

            // Is the server command blocked?
            if (Interface.uMod.CallDeprecatedHook("OnServerCommand", "OnServerCommand(string command, string[] args) or OnPlayerCommand(IPlayer player, string command, string[] args)",
                    new DateTime(2021, 12, 31), arg) != null)
            {
                return true;
            }

            if (player != null)
            {
                // Is the player command blocked?
                if (Interface.uMod.CallHook("OnServerCommand", arg.cmd.FullName, arg.Args) != null)
                {
                    return true;
                }

                // Handle the command
                string fullCommand = arg.Args?.Length > 0 ? $"{arg.cmd.FullName} {string.Join(" ", arg.Args)}" : arg.cmd.FullName;
                object[] context = ArrayPool.Get(1);
                context[0] = arg;
                try
                {
                    if (Universal.CommandSystem.HandleConsoleMessage(player, fullCommand, context) == CommandState.Completed)
                    {
                        return true;
                    }
                }
                finally
                {
                    ArrayPool.Free(context);
                }
            }

            return null;
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [Hook("OnServerInitialized")]
        private void OnServerInitialized()
        {
            // Add universal commands
            Universal.CommandSystem.DefaultCommands.Initialize(this, (player, args) =>
            {
                if (player.IsServer)
                {
                    player.Reply($"Protocol: {Server.Protocol}\nBuild Date: {BuildInfo.Current.BuildDate}\n" +
                                 $"Unity Version: {UnityEngine.Application.unityVersion}\nChangeset: {BuildInfo.Current.Scm.ChangeId}\n" +
                                 $"Branch: {BuildInfo.Current.Scm.Branch}\nuMod.Rust Version: {RustExtension.AssemblyVersion}");
                    return CommandState.Completed;
                }

                return CommandState.Unrecognized;
            });

            // Clean up invalid permission data
            permission.RegisterValidate(ExtensionMethods.IsSteamId);
            permission.CleanUp();

            if (!Universal.Config.Modded)
            {
                Interface.uMod.LogWarning("The server is currently listed under Community. Please be aware that Facepunch only allows admin tools" +
                  " (that do not affect gameplay or make the server appear modded) under the Community section"); // TODO: Localization
            }

            serverInitialized = true;
        }

        /// <summary>
        /// Called when the server description is updating
        /// </summary>
        [Hook("IOnUpdateServerDescription")]
        private void IOnUpdateServerDescription()
        {
            // Fix for server description not always updating
            SteamServer.SetKey("description_0", string.Empty);
        }

        /// <summary>
        /// Called when the server is updating Steam information
        /// </summary>
        [Hook("IOnUpdateServerInformation")]
        private void IOnUpdateServerInformation()
        {
            // Add Steam tags for uMod
            SteamServer.GameTags += ",umod";
            if (Universal.Config.Modded)
            {
                SteamServer.GameTags += ",modded";
            }
        }

        #endregion Server Hooks

        #region Deprecated Hooks

        [HookMethod("OnExperimentStart")]
        private object OnExperimentStart(Workbench workbench, BasePlayer player)
        {
            return Interface.uMod.CallDeprecatedHook("CanExperiment",
                "OnExperimentStart(Workbench workbench, BasePlayer player)",
                new DateTime(2021, 12, 31), player, workbench);
        }

        [HookMethod("OnPlayerCorpseSpawned")]
        private object OnPlayerCorpseSpawned(BasePlayer player, BaseCorpse corpse)
        {
            return Interface.uMod.CallDeprecatedHook("OnPlayerCorpse",
                "OnPlayerCorpseSpawned(BasePlayer player, BaseCorpse corpse)",
                new DateTime(2021, 12, 31), player, corpse);
        }

        [HookMethod("OnVehiclePush")]
        private object OnVehiclePush(BaseVehicle vehicle, BasePlayer player)
        {
            if (vehicle is MotorRowboat rowboat)
            {
                return Interface.uMod.CallDeprecatedHook("CanPushBoat",
                    "CanPushVehicle(BaseVehicle vehicle, BasePlayer player)",
                    new DateTime(2021, 12, 31), player, rowboat);
            }

            return null;
        }

        [HookMethod("OnFuelConsume")]
        private void OnFuelConsume(BaseOven oven, Item fuel, ItemModBurnable burnable)
        {
            Interface.uMod.CallDeprecatedHook("OnConsumeFuel",
                "OnFuelConsume(BaseOven oven, Item fuel, ItemModBurnable burnable)",
                new DateTime(2021, 12, 31), oven, fuel, burnable);
        }

        [HookMethod("OnEntitySaved")]
        private void OnEntitySaved(Elevator elevator, BaseNetworkable.SaveInfo saveInfo)
        {
            Interface.uMod.CallDeprecatedHook("OnElevatorSaved",
                "OnEntitySaved(Elevator elevator, BaseNetworkable.SaveInfo saveInfo)",
                new DateTime(2021, 12, 31), elevator, saveInfo);
        }

        [HookMethod("OnResearchCostDetermine")]
        private object OnResearchCostDetermine(Item item, ResearchTable table)
        {
            return Interface.uMod.CallDeprecatedHook("OnItemScrap", "OnResearchCostDetermine(Item item, ResearchTable table)",
                new DateTime(2021, 12, 31), table, item);
        }

        #endregion Deprecated Hooks
    }
}
